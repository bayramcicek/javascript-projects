const myObject = {
    name: "bayram",
    age: 25,
    occupation: 'jr. software engineer'
};

for (const key in myObject) {
    if (myObject.hasOwnProperty(key)) { console.log(key + ': ' + myObject[key]); }
}

const browserType = 'mozilla';
console.log(browserType.slice(1, 4)); // "ozi"

const data = 'Manchester,London,Liverpool,Birmingham,Leeds,Carlisle';

const cities = data.split(',');
console.log(cities);

// const citiesSplit = cities.join(' # ');
const citiesSplit = cities.toString(); // Manchester,London,Liverpool,Birmingham,Leeds,Carlisle
console.log(citiesSplit);

const products = [
    'Underpants:6.99',
    'Socks:5.99',
    'T-shirt:14.99',
    'Trousers:31.99',
    'Shoes:23.99',
];

