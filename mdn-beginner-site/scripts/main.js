const myHeading = document.querySelector("h1");
myHeading.textContent = "Hello, World!";

/*
let myVar = [1, 'Bob', 'Steve', 34];

document.querySelector("html").addEventListener("click", () => {
    console.log("<-clicked->");
    alert("Ouch! Stop poking me!");
});
*/

const myImage = document.querySelector('img');

myImage.onclick = () => {
    const mySrc = myImage.getAttribute("src");
    // console.log(mySrc); // images/firefox.png

    if (mySrc === "images/firefox.png") {
        myImage.setAttribute("src", "images/cat.png");
    } else {
        myImage.setAttribute("src", "images/firefox.png");
    }
};

function setUsername() {
    const myName = prompt("Please enter your name.");

    if (!myName) {
        setUsername();
    } else {
        localStorage.setItem("name", myName);
        myHeading.textContent = `Mozilla is cool, ${myName}`;
    }
}

if (!localStorage.getItem("name")) {
    setUsername();
} else {
    const storedName = localStorage.getItem("name");
    myHeading.textContent = `Mozilla is cool, ${storedName}`;
}

let myButton = document.querySelector("button");
myButton.onclick = () => {
    setUsername();
};