var anotherObject = {
	cool: function () {
		console.log("cool !!!");
	}
};

var myObj = Object.create(anotherObject);

myObj.doCool = function() {
	this.cool(); // internal delegation
};

