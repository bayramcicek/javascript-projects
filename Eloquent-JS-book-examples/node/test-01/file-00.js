/*

let { readFile } = require("fs");
readFile("./file.txt", "utf-8", (error, text) => {
    if (error) throw error;
    console.log("The file contains: ", text);
});

// $ node file-00.js
// $ The file contains:  you are reading file.txt contents...

*/

const { writeFile } = require("fs");
writeFile("graffiti.txt", "Node was here.", err => {
    if (err) console.log(`Failed to write file: ${err}`);
    else console.log("File written.");
});
// -> File written.

const { readFile } = require("fs").promises;
readFile("file.txt", "utf8")
    .then(text => console.log("The file contains:", text));
// -> The file contains: you are reading file.txt contents...