exports.reverse = function(string) {
    return Array.from(string).reverse().join("");
};

/**

$ node main.js Javascript
$ tpircsavaJ

*/