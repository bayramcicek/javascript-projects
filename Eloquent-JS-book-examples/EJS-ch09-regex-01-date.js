// ------------- date class -------//

console.log(Date.now());
console.log(new Date(2013, 11, 19).getTime());
// → 1387407600000

let today = new Date();
console.log(today.getFullYear()); // 2022

function getDate(string) {
  let [_, month, day, year] =
    /(\d{1,2})-(\d{1,2})-(\d{4})/.exec(string);
  return new Date(year, month - 1, day);
}
console.log(getDate("1-30-2003"));
// → Thu Jan 30 2003 00:00:00 GMT+0100 (CET)
console.log(/(\d{1,2})-(\d{1,2})-(\d{4})/.exec("1-30-2003"));
// ["1-30-2003", "1", "30", "2003"]
