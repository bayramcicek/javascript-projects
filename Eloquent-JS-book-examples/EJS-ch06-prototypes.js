// prototypes
let empty = {};

console.log(empty.toString); // [Function: toString]
console.log(empty.toString()); // [object Object]

console.log(Object.prototype);
// [Object: null prototype] {}
console.log(Object.getPrototypeOf({}));
// [Object: null prototype] {}

console.log(Object.getPrototypeOf(Math.max));
// {}
console.log(Function.prototype);
// {}

console.log(Object.getPrototypeOf([]));
// Object(0) []
console.log(Array.prototype);
// Object(0) []

// rabbit prototype
let protoRabbit = {
  speak(line) {
    console.log(`The ${this.type} rabbit says '${line}'`);
  }
};
let killerRabbit = Object.create(protoRabbit);
killerRabbit.type = "killer";
killerRabbit.speak("SKREEEE!");
// → The killer rabbit says 'SKREEEE!'

// ------------------------------------------------- //
function Rabbit(type) {
  this.type = type;
}
Rabbit.prototype.speak = function(line) {
  console.log(`The ${this.type} rabbit says '${line}'`);
};

let weirdRabbit = new Rabbit("weird");
weirdRabbit.speak("selam");
// The weird rabbit says 'selam'

// ---------------- //

console.log(Object.getPrototypeOf(Rabbit) == Function.prototype);
// → true
console.log(Object.getPrototypeOf(weirdRabbit) == Rabbit.prototype);
// → true
