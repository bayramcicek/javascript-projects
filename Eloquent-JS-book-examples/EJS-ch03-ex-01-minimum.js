// Minimum
// > https://eloquentjavascript.net/03_functions.html#i_XTmO7z7MPq

/*
The previous chapter introduced the standard function Math.min
that returns its smallest argument. We can build something like
that now. Write a function min that takes two arguments and returns their minimum.
*/

// my solution
function min (a, b) {
	if (a > b) return b;
	else if (a < b) return a;
	else return a;
}

console.log(min(0, 10));
// → 0
console.log(min(0, -10));
// → -10


/*
Code Sandbox solution
> https://eloquentjavascript.net/code/#3.1

function min(a, b) {
  if (a < b) return a;
  else return b;
}

*/
