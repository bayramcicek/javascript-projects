const { statSync, readdirSync, readFileSync } = require("fs");

let searchTerm = new RegExp(process.argv[2]);

for (let arg of process.argv.slice(3)) {
    search(arg);
}

function search(file) {
    let stats = statSync(file);
    if (stats.isDirectory()) {
        for (let f of readdirSync(file)) {
            search(file + "/" + f);
        }
    } else if (searchTerm.test(readFileSync(file, "utf8"))) {
        console.log(file);
    }
}

/*

$ node Eloquent-JS-book-examples/EJS-ch20-node-ex-00-search-tool.js reading ./
$ .//Eloquent-JS-book-examples/EJS-ch04-arrays-0.js
$ .//Eloquent-JS-book-examples/node/test-01/file-00.js
$ .//Eloquent-JS-book-examples/node/test-01/file.txt

*/