/*

setTimeout(
	() => console.log("Tick"),
	500 // ms (a second is a thousand milliseconds)
);

let fifteen = Promise.resolve(15);
fifteen.then(value => console.log(`Got ${value}`)); // Got 15

// Got 15
// Tick

*/

//////////////////-------------------////////////////

new Promise((_, reject) => reject(new Error("Fail")))
	.then(value => console.log("Handler 1"))
	.catch(reason => {
		console.log("Caught failure " + reason);
		return "nothing";
	})
	.then(value => console.log("Handler 2", value));
	// → Caught failure Error: Fail
	// → Handler 2 nothing
