// Everything
// > https://eloquentjavascript.net/05_higher_order.html#i_SmbRSAd5GA

/*
Analogous to the some method, arrays also have an every method.
This one returns true when the given function returns true for 
every element in the array. In a way, some is a version of the || 
operator that acts on arrays, and every is like the && operator.

Implement every as a function that takes an array and a predicate 
function as parameters. Write two versions, one using a loop and one 
using the some method.
*/

/// my solution

// using a loop
function every(array, test) {
	let counter = 0;
	for (let i = 0; i < array.length; i++) {
		if (test(array[i])) counter++;
	}
	// console.log(counter, array.length);
	return counter == array.length ? true : false;
  
}


// using the some method
function every2(array, test) {
	// failed
}

console.log(every([1, 3, 5], n => n < 10));
// → true
console.log(every([2, 4, 16], n => n < 10));
// → false
console.log(every([], n => n < 10));
// → true

// code sandbox solution
// > https://eloquentjavascript.net/code/#5.3

/*

function every(array, predicate) {
  for (let element of array) {
    if (!predicate(element)) return false;
  }
  return true;
}

function every2(array, predicate) {
  return !array.some(element => !predicate(element));
}

*/
