const { createServer } = require("http");

let server = createServer((request, response) => {
    response.writeHead(200, { "Content-Type": "text/html" });
    response.write(`
    <h1>Hello!</h1>
    <p>You asked for <code>${request.url}</code></p>`);
    response.end();
});

server.listen(8000);
console.log("Listening! (port 8000)");

/**
// -> https://nodejs.org/en/docs/guides/getting-started-guide/
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

 */