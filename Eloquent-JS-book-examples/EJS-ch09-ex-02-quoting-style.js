// > https://eloquentjavascript.net/09_regexp.html#i_dTiEW14oG0
// Quoting style

let text = "'I'm the cook,' he said, 'it's my job.'";

console.log(text.replace(/(^|\W)'|'(\W|$)/g, '$1"$2'));
// → "I'm the cook," he said, "it's my job."
