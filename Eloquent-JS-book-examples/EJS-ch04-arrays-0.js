let listOfNumbers = [2, 3, 5, 7, 11];

console.log(listOfNumbers[3]); // 7
console.log(listOfNumbers[2+2]); // 11

/*
// Almost all JavaScript values have properties.

null.length; // TypeError: Cannot read properties of null (reading 'length')

*/

console.log(listOfNumbers.length); // 5
console.log(listOfNumbers["length"]); // 5

let doh = "Doh";
console.log(typeof doh); // string
console.log(typeof doh.toUpperCase); // function
console.log(doh.toUpperCase()); // DOH

let sequence = [1, 2, 3, 4];
sequence.push(24);
console.log(sequence); // [ 1, 2, 3, 4, 24 ]
console.log(sequence.pop()); // 24
console.log(sequence); // [ 1, 2, 3, 4 ]

let day1 = {
  squirrel: false,
  events: ["work", "touched tree", "pizza", "running"]
};
console.log(day1.squirrel);
// → false
console.log(day1.wolf); // Reading a property that doesn’t exist will give you the value undefined.
// → undefined
day1.wolf = false;
console.log(day1.wolf);
// → false
console.log(day1.events); // [ 'work', 'touched tree', 'pizza', 'running' ]
console.log(day1.events[1]); // touched tree

/*

let exp = {
	property: value
};

// Properties whose names aren’t valid binding names or valid numbers have to be quoted.
let descriptions = {
  work: "Went to work",
  "touched tree": "Touched a tree"
};

*/

console.log(Object.keys(
	{
		x: 0, y: 0, z: 2
	}
	));
// → ["x", "y", "z"]

// There’s an Object.assign function that copies all properties from one object into another.
let objectA = {a: 1, b: 2};
Object.assign(objectA, {b: 3, c: 4});
console.log(objectA);
// → {a: 1, b: 3, c: 4}

/*

Arrays, then, are just a kind of object specialized for storing sequences of things.
If you evaluate typeof [], it produces "object".
You can see them as long, flat octopuses with all their tentacles in a neat row, labeled with numbers.

*/

console.log(typeof []); // object
