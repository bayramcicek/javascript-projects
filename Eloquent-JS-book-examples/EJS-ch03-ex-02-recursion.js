// Recursion
// > https://eloquentjavascript.net/03_functions.html#i_jxl1p970Fy

/*

We’ve seen that % (the remainder operator) can be used to test whether a
number is even or odd by using % 2 to see whether it’s divisible by two. 
Here’s another way to define whether a positive whole number is even or odd:

- Zero is even.

- One is odd.

- For any other number N, its evenness is the same as N - 2.

Define a recursive function isEven corresponding to this description. 
The function should accept a single parameter (a positive, whole number) and return a Boolean.

Test it on 50 and 75. See how it behaves on -1. Why? Can you think of a way to fix this?

*/
/*

// my solution for positive int.
let isEven = function(n) {
	if (n == 0) return true;
	else if (n == 1) return false;
	else {
		return isEven(n - 2);
	}
};

console.log(isEven(50));
// → true
console.log(isEven(75));
// → false
// console.log(isEven(-1));
// → RangeError: Maximum call stack size exceeded

*/

// my solution for positive and negative integers.
let isEven = function(n) {
	if (n == 0) return true;
	else if (n == 1) return false;
	else if (n < 0) return isEven(n + 2);
	else {
		return isEven(n - 2);
	}
};

console.log(isEven(50));
// → true
console.log(isEven(75));
// → false
console.log(isEven(-1));
// → false
console.log(isEven(-60));
// true

// Code Sandbox solution
// > https://eloquentjavascript.net/code/#3.2

/*

function isEven(n) {
  if (n == 0) return true;
  else if (n == 1) return false;
  else if (n < 0) return isEven(-n);
  else return isEven(n - 2);
}

console.log(isEven(50));
// → true
console.log(isEven(75));
// → false
console.log(isEven(-1));
// → false

*/
