/*
> https://eloquentjavascript.net/05_higher_order.html#i_aIOczlLyX1
Flattening

Use the reduce method in combination with the concat method to “flatten” an array of arrays into a single array that has all the elements of the original arrays.

let arrays = [[1, 2, 3], [4, 5], [6]];
// Your code here.
// → [1, 2, 3, 4, 5, 6]

*/

// my solution
let arrays = [[1, 2, 3], [4, 5], [6]];

let flatArray = function (arr) {
	return arr.reduce((a, b) => a.concat(b));
};

console.log(flatArray(arrays));
// [ 1, 2, 3, 4, 5, 6 ]

// Code Sandbox solution
// > https://eloquentjavascript.net/code/#5.1
console.log(arrays.reduce((flat, current) => flat.concat(current), []));
// → [1, 2, 3, 4, 5, 6]
