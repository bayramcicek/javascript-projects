/*
> https://eloquentjavascript.net/02_program_structure.html#i_umoXp9u0e7

Looping a triangle
Write a loop that makes seven calls to console.log to output the following triangle:

#
##
###
####
#####
######
#######

*/

var hashtag = "";
for(let i = 0; i < 7; i++) {
	hashtag += "#"
	console.log(hashtag);
}

/*
Code Sandbox solution: https://eloquentjavascript.net/code/#2.1

for (let line = "#"; line.length < 8; line += "#")
  console.log(line);
*/