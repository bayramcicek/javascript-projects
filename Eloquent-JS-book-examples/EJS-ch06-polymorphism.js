/*
let Rabbit = new Object();
Rabbit.prototype.toString = function() {
  return `a ${this.type} rabbit`;
};

console.log(String(blackRabbit));
// → a black rabbit
*/

// symbols
const toStringSymbol = Symbol("toString");
Array.prototype[toStringSymbol] = function() {
  return `${this.length} cm of blue yarn`;
};

console.log([1, 2].toString());
// → 1,2
console.log([1, 2][toStringSymbol]());
// → 2 cm of blue yarn

/*
It is possible to include symbol properties in object expressions and 
classes by using square brackets around the property name. 
That causes the property name to be evaluated, much like the 
square bracket property access notation, which allows us to refer 
to a binding that holds the symbol.
*/

let stringObject = {
  [toStringSymbol]() { return "a jute rope"; }
};
console.log(stringObject[toStringSymbol]());
// → a jute rope
