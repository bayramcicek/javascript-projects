let x = 10;
if (true) {
  let y = 20;
  var z = 30;
  console.log(x + y + z);
  // → 60
}
// let y is not visible here
// var z is visible
console.log(x + z);
// → 40

let launchMissiles = function() {
  console.log("missile launched");
};

launchMissiles();

// They are conceptually moved to the top of their scope and can be used by all the code in that scope.
function square(argument) {
  return argument * argument;
}
console.log(square(5));


const power = (base, exponent) => {
  let result = 1;
  for (let count = 0; count < exponent; count++) {
    result *= base;
  }
  return result; 
};

const car = x => x + 10;
console.log(car(1));

const empty = () => 5;
console.log(empty());