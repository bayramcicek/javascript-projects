console.log("coconuts".slice(4, 7));
// → nut
console.log("cocuonutu".indexOf("u")); // first encounter
// → 3

console.log("one eetwo three".indexOf("ee"));
// → 4

// The trim method removes whitespace (spaces, newlines, tabs, and similar characters)
// from the start and end of a string.

console.log("  okay \n ".trim());
// → okay

console.log(String(65).padStart(4, "0"));
// → 0065

let sentence = "Secretarybirds specialize in stomping";
let words = sentence.split(" ");
console.log(words);
// → ["Secretarybirds", "specialize", "in", "stomping"]
console.log(words.join(". "));
// → Secretarybirds. specialize. in. stomping

console.log("LA".repeat(3));
// → LALALA