// > https://eloquentjavascript.net/03_functions.html#p_mWzvA1dWtJ

function findSolution(target) {
  
  // ------------------------------- //
  function find(current, history) { 
    // console.log(current, history);
    if (current == target) {
      return history;
    } else if (current > target) {
      return null;
    } else {
      return find(current + 5, `(${history} + 5)`) ||
             find(current * 3, `(${history} * 3)`);
    }
  }
  // ------------------------------- //
  
  return find(1, "1");
}

console.log(findSolution(24));
// → (((1 * 3) + 5) * 3)

// ----------------------------------- function types ---- //

// Define f to hold a function value
const f = function(a) {
  console.log(a + 2);
};

// Declare g to be a function
function g(a, b) {
  return a * b * 3.5;
}

// A less verbose function value
let h = a => a % 3;


/*

A key aspect in understanding functions is understanding scopes.
Each block creates a new scope. Parameters and bindings declared in
a given scope are local and not visible from the outside.
Bindings declared with var behave differently—they end up in
the nearest function scope or the global scope.

*/
