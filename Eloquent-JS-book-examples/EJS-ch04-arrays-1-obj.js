let object1 = {value: 10};
let object2 = object1;
let object3 = {value: 10};

console.log(object1 == object2);
// → true
console.log(object1 == object3);
// → false

object1.value = 15;
console.log(object2.value);
// → 15
console.log(object3.value);
// → 10

const score = {visitors: 0, home: 0};
// This is okay
score.visitors = 1;

// This isn't allowed
// score = {visitors: 1, home: 1}; // TypeError: Assignment to constant variable.

// phi coefficient (ϕ).
// > https://eloquentjavascript.net/04_data.html#p_mq//xARuKm

function phi(table) {
  return (table[3] * table[0] - table[2] * table[1]) /
    Math.sqrt((table[2] + table[3]) *
              (table[0] + table[1]) *
              (table[1] + table[3]) *
              (table[0] + table[2]));
}

console.log(phi([76, 9, 4, 1]));
// → 0.068599434
