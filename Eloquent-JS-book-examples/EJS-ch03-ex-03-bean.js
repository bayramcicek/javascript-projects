// Bean counting
// > https://eloquentjavascript.net/03_functions.html#i_3rsiDgC2do

/*
You can get the Nth character, or letter, from a string by writing "string"[N]. 
The returned value will be a string containing only one character (for example, "b"). 
The first character has position 0, which causes the last one to be found at position 
string.length - 1. In other words, a two-character string has length 2, and its characters have positions 0 and 1.

Write a function countBs that takes a string as its only argument and returns a number
 that indicates how many uppercase “B” characters there are in the string.

Next, write a function called countChar that behaves like countBs, except it takes 
a second argument that indicates the character that is to be counted (rather than
 counting only uppercase “B” characters). Rewrite countBs to make use of this new function.
*/

/*

let a = "selamlAr";
// let res = a[0];
// console.log(res); // s
console.log('A' == a[6]); // true

*/

// my solution for countBs
let countBs = (stringB) => {
	let counter = 0;
	for (let i = 0; i < stringB.length; i++) {
		if (stringB[i] == 'B') counter++;
	}
	return counter;
};

console.log(countBs("BBBBBB")); // 6

// my solution for countChar
let countChar = (string, char) => {
	let counter = 0;
	for (let i = 0; i < string.length; i++) {
		if (string[i] == char) counter++;
	}
	return counter;
};

console.log(countChar("ElEphant123", 'E')); // 2
console.log(countChar("kakkerlak", "k")); // 4

/*
Code Sandbox solution
> https://eloquentjavascript.net/code/#3.3

function countChar(string, ch) {
  let counted = 0;
  for (let i = 0; i < string.length; i++) {
    if (string[i] == ch) {
      counted += 1;
    }
  }
  return counted;
}

function countBs(string) {
  return countChar(string, "B");
}

console.log(countBs("BBC"));
// → 2
console.log(countChar("kakkerlak", "k"));
// → 4

*/
