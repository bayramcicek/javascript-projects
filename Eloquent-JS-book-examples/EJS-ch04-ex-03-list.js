// A list
// https://eloquentjavascript.net/04_data.html#i_nSTX34CM1M

// my solution (incomplete)
let arrayToList = function(arr) {
	let list = {};
	for (let i = arr.length - 1; i >= 0; i--) {
		// console.log(arr[i]);
		list = {value: arr[i], rest: list};
	}
	return list;
};

let listToArray = (list) => {
	let arr = [];
	for (let node = list; node.rest != null; node = node.rest) {
		arr.push(node.value);
	}
	return arr;
};

let array = [1,2,3];
let currenList = arrayToList(array); 
console.log(currenList);
// { value: 1, rest: { value: 2, rest: { value: 3, rest: {} } } }

let currentArray = listToArray(currenList);
console.log(currentArray); // [ 1, 2, 3 ]

/*
Code Sandbox solution
> https://eloquentjavascript.net/code/#4.3

function arrayToList(array) {
  let list = null;
  for (let i = array.length - 1; i >= 0; i--) {
    list = {value: array[i], rest: list};
  }
  return list;
}

function listToArray(list) {
  let array = [];
  for (let node = list; node; node = node.rest) {
    array.push(node.value);
  }
  return array;
}

function prepend(value, list) {
  return {value, rest: list};
}

function nth(list, n) {
  if (!list) return undefined;
  else if (n == 0) return list.value;
  else return nth(list.rest, n - 1);
}

console.log(arrayToList([10, 20]));
// → {value: 10, rest: {value: 20, rest: null}}
console.log(listToArray(arrayToList([10, 20, 30])));
// → [10, 20, 30]
console.log(prepend(10, prepend(20, null)));
// → {value: 10, rest: {value: 20, rest: null}}
console.log(nth(arrayToList([10, 20, 30]), 1));
// → 20

*/
