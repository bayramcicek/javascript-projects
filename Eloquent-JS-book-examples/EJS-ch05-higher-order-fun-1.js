/*
The some method is another higher-order function. 
It takes a test function and tells you whether that
function returns true for any of the elements in the array.


function characterScript(code) {
  for (let script of SCRIPTS) {
    if (script.ranges.some(([from, to]) => {
      return code >= from && code < to;
    })) {
      return script;
    }
  }
  return null;
}

console.log(characterScript(121));
// → {name: "Latin", …}

*/

// > https://eloquentjavascript.net/05_higher_order.html#p_AEbxGojOu6
// Two emoji characters, horse and shoe
let horseShoe = "🐴👟";
console.log(horseShoe.length);
// → 4
console.log(horseShoe[0]);
// → (Invalid half-character)
console.log(horseShoe.charCodeAt(0));
// → 55357 (Code of the half-character)
console.log(horseShoe.codePointAt(0));
// → 128052 (Actual code for horse emoji)

let roseDragon = "🌹🐉";
for (let char of roseDragon) {
  console.log(char);
  console.log(char.codePointAt(0));
  
}
// → 🌹
// → 🐉

/*
It uses another array method—findIndex.
This method is somewhat like indexOf, but instead of
looking for a specific value, it finds the first value
or which the given function returns true. Like indexOf, 
it returns -1 when no such element is found.
*/

/*
// > https://eloquentjavascript.net/05_higher_order.html#p_T2D/Ix5YaM
function textScripts(text) {
  let scripts = countBy(text, char => {
    let script = characterScript(char.codePointAt(0));
    return script ? script.name : "none";
  }).filter(({name}) => name != "none");

  let total = scripts.reduce((n, {count}) => n + count, 0);
  if (total == 0) return "No scripts found";

  return scripts.map(({name, count}) => {
    return `${Math.round(count * 100 / total)}% ${name}`;
  }).join(", ");
}

console.log(textScripts('英国的狗说"woof", 俄罗斯的狗说"тяв"'));
// → 61% Han, 22% Latin, 17% Cyrillic
*/

