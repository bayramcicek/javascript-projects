/*
> https://eloquentjavascript.net/02_program_structure.html#i_swb9JBtSQQ
Chessboard
Write a program that creates a string that represents an 8×8 grid, 
using newline characters to separate lines. At each position of the grid 
there is either a space or a "#" character. The characters should form a chessboard.

Passing this string to console.log should show something like this:

 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # # 
 # # # #
# # # #

When you have a program that generates this pattern, define a binding 
size = 8 and change the program so that it works for any size, outputting a
 grid of the given width and height.
*/

let size = 8;

for (let i = 0; i < size; i++ ) {
	
	let row = "";

	if (i % 2 == 0) {
		for (let j = 0; j < size; j++) {
			if (j % 2 == 0) {
				row += " ";
				
			} else {
				row += "#";
			}
		}
		console.log(row);
	} else {
		for (let j = 0; j < size; j++) {
			if (j % 2 == 0) {
				row += "#";
				
			} else {
				row += " ";
			}
		}
		console.log(row);
	}
}

/*
> https://eloquentjavascript.net/code/#2.3
> Code Sandbox solution:

let size = 8;

let board = "";

for (let y = 0; y < size; y++) {
  for (let x = 0; x < size; x++) {
    if ((x + y) % 2 == 0) {
      board += " ";
    } else {
      board += "#";
    }
  }
  board += "\n";
}

console.log(board);



*/
