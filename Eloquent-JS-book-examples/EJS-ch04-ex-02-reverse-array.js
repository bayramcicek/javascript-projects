/*
Reversing an array
> https://eloquentjavascript.net/04_data.html#i_6xTmjj4Rf5

Arrays have a reverse method that changes the array by inverting the
 order in which its elements appear. For this exercise, write two functions, 
 reverseArray and reverseArrayInPlace. The first, reverseArray, takes an array 
 as argument and produces a new array that has the same elements in the inverse 
 order. The second, reverseArrayInPlace, does what the reverse method does: 
 it modifies the array given as argument by reversing its elements. Neither 
 may use the standard reverse method.

Thinking back to the notes about side effects and pure functions in 
the previous chapter, which variant do you expect to be useful in more 
situations? Which one runs faster?

*/

function reverseArray(arr) {
	let newArr = [];
	for (let i of arr) {
		newArr.unshift(i);
	}
	return newArr;
}

function reverseArrayInPlace(arr) {
	let temp = reverseArray(arr);
	for (let i = 0; i < temp.length; i++) {
		arr[i] = temp[i];
	}
}

let firstArray = [1,2,3,4,5]
console.log(reverseArray(firstArray)); // [ 5, 4, 3, 2, 1 ]

let secondArray = ["a", "b", "f", "z"];
reverseArrayInPlace(secondArray);
console.log(secondArray); // [ 'z', 'f', 'b', 'a' ]




/*
Code Sandbox solution
> https://eloquentjavascript.net/code/#4.2

function reverseArray(array) {
  let output = [];
  for (let i = array.length - 1; i >= 0; i--) {
    output.push(array[i]);
  }
  return output;
}

function reverseArrayInPlace(array) {
  for (let i = 0; i < Math.floor(array.length / 2); i++) {
    let old = array[i];
    array[i] = array[array.length - 1 - i];
    array[array.length - 1 - i] = old;
  }
  return array;
}

console.log(reverseArray(["A", "B", "C"]));
// → ["C", "B", "A"];
let arrayValue = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayValue);
console.log(arrayValue);
// → [5, 4, 3, 2, 1]

*/
