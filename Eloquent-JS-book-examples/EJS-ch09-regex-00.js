// testing for matches
console.log(/abc/.test("xwabcty")); // true
console.log(/abc/.test("xabxc")); // false

// Sets of characters
console.log(/[0123456789]/.test("in 1992")); // → true
console.log(/[0-9]/.test("in 1992")); // → true
// \d means the same thing as [0-9].

let dateTime = /\d\d-\d\d-\d\d\d\d \d\d:\d\d/;
console.log(dateTime.test("01-30-2003 15:20"));
// → true
console.log(dateTime.test("30-jan-2003 15:20"));
// → false

let notBinary = /[^01]/;
console.log(notBinary.test("1100100010100110"));
// → false
console.log(notBinary.test("1100100010200110"));
// → true

let dateTime1 = /\d{1,2}-\d{1,2}-\d{4} \d{1,2}:\d{2}/;
console.log(dateTime1.test("1-30-2003 8:45"));
// → true

let cartoonCrying = /boo+(hoo+)+/i;
console.log(cartoonCrying.test("Boohoooohoohooo"));
// → true

/*
The i at the end of the expression in the example makes this regular expression case insensitive,
 allowing it to match the uppercase B in the input string,
 even though the pattern is itself all lowercase.
*/

// Matches and groups
let match = /\d+/.exec("one two 100");
console.log(match); // → ["100"]
console.log(match.index); // → 8
