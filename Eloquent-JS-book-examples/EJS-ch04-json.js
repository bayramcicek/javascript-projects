/*

JavaScript gives us the functions JSON.stringify and JSON.parse to 
convert data to and from this format. The first takes a JavaScript 
value and returns a JSON-encoded string. The second takes such a
string and converts it to the value it encodes.

*/

// JS to JSON
let string = JSON.stringify({
	squirrel: false,
	events: ["weekend"]
	
});

console.log(string);
// → {"squirrel":false,"events":["weekend"]}

// JSON to JS
let myObj = JSON.parse(string);
console.log(myObj.events);
// → ["weekend"]
console.log(myObj);
// { squirrel: false, events: [ 'weekend' ] }
