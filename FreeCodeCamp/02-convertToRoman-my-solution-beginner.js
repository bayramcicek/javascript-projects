/*
 * created by cicek on Mar 05, 2022 2:20 PM
 *
 * */

 /*
  *
https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/javascript-algorithms-and-data-structures-projects/roman-numeral-converter

  Roman Numeral Converter
Convert the given number into a roman numeral.

All roman numerals answers should be provided in upper-case.

  */

 function convertToRoman(num) {

  let resArray = [];
  let romanThousand = 1000 * Math.floor(num / 1000);
  let romanHundred = 100 * Math.floor(num / 100) - romanThousand;
  let romanTen = 10 * Math.floor(num / 10) - romanThousand - romanHundred;
  let romanUnary = num - (romanThousand + romanHundred + romanTen);


  console.log(romanThousand);
  console.log(romanHundred);
  console.log(romanTen);
  console.log(romanUnary);

  if (romanThousand !== 0)
  {
    let t = romanThousand/ 1000;
    let str = '';
    for (let i = 0; i < t; i++)
      str += 'M';

    resArray.push(str);
  }

  if (romanHundred !== 0)
  {
    let h = romanHundred / 100;

    if (h == 4)
      resArray.push('CD');

    else if (h == 9)
      resArray.push('CM');

    else if (h == 5)
      resArray.push('D');
    else
    {
      let str = '';

      if (h < 5)
        for (let i = 0; i<h; i++)
          str += 'C';
      else
      {
        str = 'D';
        for (let i = 0; i<h-5; i++)
          str += 'C';
      }
      resArray.push(str);
    }
  }

  if (romanTen !== 0)
  {
    let t = romanTen / 10;

    if (t == 4)
      resArray.push('XL');

    else if (t == 9)
      resArray.push('XC');

    else if (t == 5)
      resArray.push('L');
    else
    {
      let str = '';

      if (t < 5)
        for (let i = 0; i<t; i++)
          str += 'X';
      else
      {
        str = 'L';
        for (let i = 0; i<t-5; i++)
          str += 'X';
      }
      resArray.push(str);
    }
  }

  if (romanUnary !== 0)
  {
    let u = romanUnary;

    if (u == 4)
      resArray.push('IV');

    else if (u == 9)
      resArray.push('IX');

    else if (u == 5)
      resArray.push('V');
    else
    {
      let str = '';

      if (u < 5)
        for (let i = 0; i<u; i++)
          str += 'I';
      else
      {
        str = 'V';
        for (let i = 0; i<u-5; i++)
          str += 'I';
      }
      resArray.push(str);
    }
  }

/*
  let lastCalculation = () => {
    let a = '';
    for (let i = 0; i < resArray.length; i++)
      a += resArray[i];
    return a;
  };
*/

let concatCalculation = '';
for (let i = 0; i < resArray.length; i++)
  concatCalculation += resArray[i];

 return concatCalculation;
}

let a = convertToRoman(2784); // MM DCC LXXX IV
console.log(a);

/*
 convertToRoman(500) should return the string D

convertToRoman(501) should return the string DI

convertToRoman(649) should return the string DCXLIX

convertToRoman(798) should return the string DCCXCVIII

convertToRoman(891) should return the string DCCCXCI

convertToRoman(1000) should return the string M

convertToRoman(1004) should return the string MIV

convertToRoman(1006) should return the string MVI

convertToRoman(1023) should return the string MXXIII

convertToRoman(2014) should return the string MMXIV

convertToRoman(3999) should return the string MMMCMXCIX*/
