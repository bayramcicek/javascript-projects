/* from: https://www.mathsisfun.com/roman-numerals.html */

// advanced

function Level(i,v,x)
{
  this.i=i
  this.v=v
  this.x=x
}

let levels=[]
levels[0]=new Level('I','V','X')
levels[1]=new Level('X','L','C')
levels[2]=new Level('C','D','M')

function calcDigit(d,k)
{
  if(k>2)
  {
    let str=''
    for(let m=1;m<=d*Math.pow(10,k-3);m++)str+='M'
    return str
  }
  else if(d==1)return levels[k].i
  else if(d==2)return levels[k].i+levels[k].i
  else if(d==3)return levels[k].i+levels[k].i+levels[k].i
  else if(d==4)return levels[k].i+levels[k].v
  else if(d==5)return levels[k].v
  else if(d==6)return levels[k].v+levels[k].i
  else if(d==7)return levels[k].v+levels[k].i+levels[k].i
  else if(d==8)return levels[k].v+levels[k].i+levels[k].i+levels[k].i
  else if(d==9)return levels[k].i+levels[k].x
  else return ''
}

function convertToRoman(m)
{
let r='';
let n = m.toString();
for(let c=0;c<n.length;c++)
  r+=calcDigit(eval(n.charAt(c)),n.length-c-1)
return r;
}

let a = convertToRoman(2022);
console.log(a);
