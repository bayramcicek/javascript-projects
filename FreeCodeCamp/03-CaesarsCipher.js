/*
 * created by cicek on Mar 05, 2022 6:13 PM
 *
 * */

/*
https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/javascript-algorithms-and-data-structures-projects/caesars-cipher

Caesars Cipher
One of the simplest and most widely known ciphers is a Caesar cipher, also known as a shift cipher. In a shift cipher the meanings of the letters are shifted by some set amount.

A common modern use is the ROT13 cipher, where the values of the letters are shifted by 13 places. Thus A ↔ N, B ↔ O and so on.

Write a function which takes a ROT13 encoded string as input and returns a decoded string.

All letters will be uppercase. Do not transform any non-alphabetic character (i.e. spaces, punctuation), but do pass them on.

*/

function rot13(str) {

  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let strArray = [...str];
  // console.log(strArray);

  let res = strArray.map(item =>
  {
    let currIndex = alphabet.indexOf(item);

    if (currIndex !== -1)
    {
      return alphabet[(currIndex+13) % 26];
    }

    return item;
  });

  //console.log(res);
  //console.log(res.join(''));

  return res.join('');
}

rot13("SERR PBQR PNZC?");

/*

rot13("SERR PBQR PNZC") should decode to the string FREE CODE CAMP

rot13("SERR CVMMN!") should decode to the string FREE PIZZA!

rot13("SERR YBIR?") should decode to the string FREE LOVE?

rot13("GUR DHVPX OEBJA SBK WHZCF BIRE GUR YNML QBT.") should decode to the string THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG.

*/
